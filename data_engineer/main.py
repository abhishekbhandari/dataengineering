import pandas as pd
from fastparquet import ParquetFile
__doc__ = """Reads data from parquet and csv File To join and create a json file in batch"""
class Task1:
    __doc__ = """Reads data from parquet and csv File To join and create a json file in batch""" 
    def __init__(self):
        """read the files and performs merge operation"""
        self.chunck_size=200
    def read_parquet_file(self,filename):
        """return a parquet File"""
        print(self.chunck_size)
        return ParquetFile(filename)
    def get_stu_in_chuncks(self,filename):
        """Return a TextFileReader obj contains csv file data in batchs"""
        return pd.read_csv(filename,sep='_',chunksize=self.chunck_size)
    def merge_df(self,df1,df2,common):
        """joins two dataframes based on common attribute"""
        print(self.chunck_size)
        #print(df1.head(10).to_dict())
        #print(df2.head(5).to_dict())
        return pd.merge(df1,df2,how="inner",on=common)
    def task1(self):
        """read the files in batches and performs merge operation and save json file in dir"""
        index=1
        teachers = self.read_parquet_file('teachers.parquet')
        students=self.get_stu_in_chuncks('students.csv')
        for studentdf in students:
            for teacherdf in teachers.iter_row_groups():
                result=self.merge_df(studentdf,teacherdf,common=["cid"])
                result=result.to_json(f'data{index}.json',orient='index')
                index=index+1


if __name__=="__main__":
    obj=Task1()
    obj.task1()
