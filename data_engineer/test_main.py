import json
import unittest
import os
from main import Task1
from fastparquet import ParquetFile
import pandas as pd
from pandas.io.parsers.readers import TextFileReader
class TestStringMethods(unittest.TestCase):
    
    def test_FileCreated(self):
       # obj=Task1()
       # obj.task1()
        self.assertTrue(os.path.exists('data1.json'))
        self.assertTrue(os.path.exists('data2.json'))
        self.assertTrue(os.path.exists('data3.json'))
        self.assertTrue(os.path.exists('data4.json'))
        self.assertTrue(os.path.exists('data5.json'))

    def test_read_parquet_file(self):
        obj=Task1()
        self.assertIsNotNone(obj.read_parquet_file('teachers.parquet'))
        self.assertTrue(str(type(obj.read_parquet_file('teachers.parquet')))=="<class 'fastparquet.api.ParquetFile'>")

    def test_get_stu_in_chuncks(self):
        obj=Task1()
        self.assertIsNotNone(obj.get_stu_in_chuncks('students.csv'))
        self.assertIsInstance(obj=obj.get_stu_in_chuncks('students.csv'),cls=TextFileReader)
        
    def test_merge_df(self):
        obj=Task1()
        dict1={'id': {800: 801, 801: 802, 802: 803, 803: 804, 804: 805, 805: 806, 806: 807, 807: 808, 808: 809, 809: 810}, 'fname': {800: 'Michele', 801: 'Bill', 802: 'Carey', 803: 'Casie', 804: 'Fletcher', 805: 'Evin', 806: 'Patrizia', 807: 'Tiena', 808: 'Rochell', 809: 'Cirstoforo'}, 'lname': {800: 'Durker', 801: 'Miquelet', 802: 'Poyzer', 803: 'Grunguer', 804: 'Hoovart', 805: 'Jobke', 806: 'Chisholm', 807: 'Drinkhill', 808: 'Bailes', 809: 'Merrydew'}, 'email': {800: 'mdurkerm8@xinhuanet.com', 801: 'bmiqueletm9@cdbaby.com', 802: 'cpoyzerma@furl.net', 803: 'cgrunguermb@woothemes.com', 804: 'fhoovartmc@smh.com.au', 805: 'ejobkemd@ox.ac.uk', 806: 'pchisholmme@hao123.com', 807: 'tdrinkhillmf@oracle.com', 808: 'rbailesmg@wufoo.com', 809: 'cmerrydewmh@hp.com'}, 'ssn': {800: '853-74-3710', 801: '318-72-6352', 802: '808-62-6957', 803: '194-97-3000', 804: '721-80-4864', 805: '546-96-6079', 806: '362-03-2466', 807: '498-52-5195', 808: '200-15-5454', 809: '487-12-7069'}, 'address': {800: '50 Lotheville Pass', 801: '469 Declaration Way', 802: '037 Bay Trail', 803: '83 Colorado Way', 804: '97 Garrison Park', 805: '5 Erie Road', 806: '269 Iowa Lane', 807: '43 Crest Line Hill', 808: '1832 Anniversary Parkway', 809: '9 Hauk Avenue'}, 'cid': {800: '08-2046381', 801: '57-9105495', 802: '76-3364242', 803: '32-3782498', 804: '50-4537323', 805: '83-0287469', 806: '86-8652269', 807: '76-2796204', 808: '02-4026245', 809: '52-0676084'}}
        dict2={'id': {0: '51-3016916', 1: '03-1224783', 2: '33-3280005', 3: '19-7014703', 4: '76-7924467'}, 'fname': {0: 'Jessa', 1: 'Tate', 2: 'Trenna', 3: 'Rabbi', 4: 'Amalle'}, 'lname': {0: 'Gibbs', 1: 'Weekley', 2: 'Chasney', 3: 'McGuinness', 4: 'Toffetto'}, 'email': {0: 'jgibbs0@bandcamp.com', 1: 'tweekley1@nps.gov', 2: 'tchasney2@vimeo.com', 3: 'rmcguinness3@bloglines.com', 4: 'atoffetto4@ftc.gov'}, 'ssn': {0: '733-04-9225', 1: '392-55-5726', 2: '608-97-4356', 3: '548-85-3011', 4: '598-24-7742'}, 'address': {0: '32 Ridgeview Circle', 1: '5 Lawn Alley', 2: '4 Sunbrook Crossing', 3: '12 Sachtjen Street', 4: '08 Anzinger Parkway'}, 'cid': {0: '08-2046381', 1: '57-9105495', 2: '76-3364242', 3: '32-3782498', 4: '50-4537323'}}
        
        df1=pd.DataFrame(dict1)
        df2=pd.DataFrame(dict2)
        expected_df={'id_x': {0: 801, 1: 802, 2: 803, 3: 804, 4: 805}, 'fname_x': {0: 'Michele', 1: 'Bill', 2: 'Carey', 3: 'Casie', 4: 'Fletcher'}, 'lname_x': {0: 'Durker', 1: 'Miquelet', 2: 'Poyzer', 3: 'Grunguer', 4: 'Hoovart'}, 'email_x': {0: 'mdurkerm8@xinhuanet.com', 1: 'bmiqueletm9@cdbaby.com', 2: 'cpoyzerma@furl.net', 3: 'cgrunguermb@woothemes.com', 4: 'fhoovartmc@smh.com.au'}, 'ssn_x': {0: '853-74-3710', 1: '318-72-6352', 2: '808-62-6957', 3: '194-97-3000', 4: '721-80-4864'}, 'address_x': {0: '50 Lotheville Pass', 1: '469 Declaration Way', 2: '037 Bay Trail', 3: '83 Colorado Way', 4: '97 Garrison Park'}, 'cid': {0: '08-2046381', 1: '57-9105495', 2: '76-3364242', 3: '32-3782498', 4: '50-4537323'}, 'id_y': {0: '51-3016916', 1: '03-1224783', 2: '33-3280005', 3: '19-7014703', 4: '76-7924467'}, 'fname_y': {0: 'Jessa', 1: 'Tate', 2: 'Trenna', 3: 'Rabbi', 4: 'Amalle'}, 'lname_y': {0: 'Gibbs', 1: 'Weekley', 2: 'Chasney', 3: 'McGuinness', 4: 'Toffetto'}, 'email_y': {0: 'jgibbs0@bandcamp.com', 1: 'tweekley1@nps.gov', 2: 'tchasney2@vimeo.com', 3: 'rmcguinness3@bloglines.com', 4: 'atoffetto4@ftc.gov'}, 'ssn_y': {0: '733-04-9225', 1: '392-55-5726', 2: '608-97-4356', 3: '548-85-3011', 4: '598-24-7742'}, 'address_y': {0: '32 Ridgeview Circle', 1: '5 Lawn Alley', 2: '4 Sunbrook Crossing', 3: '12 Sachtjen Street', 4: '08 Anzinger Parkway'}}
        self.assertDictContainsSubset(subset=expected_df,dictionary=obj.merge_df(df1,df2,common=["cid"]).to_dict())
    
if __name__ == '__main__':
    unittest.main()